<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 18.01.2019
 * Time: 19:02
 */

require_once __DIR__ . '/vendor/autoload.php';

?>

<link rel="stylesheet" href="/web/medien/assets/font-awesome/css/all.min.css">
<link rel="stylesheet" href="/web/medien/assets/fotos/build/medien_fotos.css">
<div id="fotos"></div>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.21/vue.js"></script>-->
<script src="/web/medien/assets/vue.min.js"></script>
<script src="/web/medien/assets/vue-router.min.js"></script>
<script src="/web/medien/assets/vuex.min.js"></script>
<script>var medien_fotos_settings = <?php echo json_encode(\JUZE\Medien\Settings\Settings::getInstance()->get()); ?>;</script>
<script src="/web/medien/assets/fotos/build/medien_fotos.js"></script>

<?php
