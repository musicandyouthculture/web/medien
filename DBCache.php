<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 08.06.19
 * Time: 00:12
 */

namespace JUZE\Medien;

class DBCache
{
    private $namespace;
    private $filename;
    private $time;
    private $filepath;
    private $data;

    /**
     * @var \mysqli
     */
    private $conn;

    /**
     * Cache constructor.
     *
     * @param \mysqli $conn
     * @param string  $namespace
     * @param string  $filename
     * @param int     $time
     */
    public function __construct($conn, $namespace, $filename, $time)
    {
        $this->conn = $conn;
        $this->namespace = $namespace;
        $this->filename = $filename;
        $this->time = $time;

        $this->filepath = "$namespace-$filename";
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @param string $filepath
     *
     * @return $this
     */
    public function setFilePath($filepath)
    {
        $this->filepath = $filepath;

        return $this;
    }

    /**
     * @param mixed $data
     *
     * @return bool|int
     */
    public function setData($data)
    {
        $this->data = $data;

        $stmt = $this->conn->prepare("
            INSERT INTO gigadmin.cache VALUES (?, ?, ?)
            ON DUPLICATE KEY UPDATE
            `value` = ?,
            `timestamp` = ?
            ;
        ");
        $stmt->bind_param(
            'ssisi',
            $this->filepath,
            $data,
            time(),
            $data,
            time()
        );
        return $stmt->execute();

        # return file_put_contents($this->filepath, $data);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        // TODO: check up on time comparison so we can set $time to strtotime("5 minutes") instead of the negative inverse

        $file = $this->query();

        return
            $file !== false &&
            $this->time < $file['timestamp'];
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return ! $this->isValid();
    }

    /**
     * @return bool|string
     */
    public function getData()
    {
        if ($this->data ?? false) {
            return $this->data;
        }

        return $this->query()['value'];
    }

    /**
     * @return array|bool|null
     */
    private function query()
    {
        $stmt = $this->conn->prepare("SELECT `value`,`timestamp` FROM gigadmin.cache WHERE `key` = ? LIMIT 1");
        $stmt->bind_param('s', $this->filepath);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result ? $result->fetch_assoc() : false;
    }
}
