<?php

namespace JUZE\Medien\Flickr;

use GuzzleHttp\Client as GuzzleClient;
use JUZE\Medien\Cache;
use JUZE\Medien\DBCache;
use JUZE\Medien\Utilities\Utilities;

class Flickr
{
    public const API_URL = 'https://api.flickr.com/services/rest/';

    /**
     * @link https://www.flickr.com/services/api/flickr.photos.getSizes.html
     *
     * @var array
     */
    public const SIZES = [
        'Square' => 's',
        'Large Square' => 'q',
        'Thumbnail' => 't',
        'Small' => 'm',
        'Small 320' => 'n',
        'Medium' => '',
        'Medium 640' => 'z',
        'Medium 800' => 'c',
        'Large' => 'b',
        'Original' => 'o',
    ];

    /**
     * @var GuzzleClient
     */
    private static $client;

    /**
     * @return GuzzleClient
     */
    private static function getClient()
    {
        if (! self::$client instanceof GuzzleClient) {
            self::$client = new GuzzleClient([
                'base_uri' => self::API_URL,
            ]);
        }

        return self::$client;
    }

    /**
     * @param string $method
     * @param array  $params
     *
     * @return int|mixed|\Psr\Http\Message\ResponseInterface
     */
    public static function rawQuery($method, $params = [])
    {
        $client = self::getClient();

        try {
            $response = $client->request(
                'GET',
                '',
                [
                    'query' => [
                        'method' => $method,
                        'api_key' => \Env::get('FLICKR_API_KEY'),
                        'format' => 'json',
                        'nojsoncallback' => 1,
                    ] + $params,
                ]
            );
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return $e->getCode();
        }

        return $response;
    }

    /**
     * @param string $method
     * @param array  $params
     *
     * @return string
     */
    public static function query($method, $params = [])
    {
        global $conn;

        $cache = new DBCache(
            $conn,
            'cache', "flickr-$method-" . Utilities::hashApiParams($params) . '.json',
            strtotime(\Env::get('FLICKR_API_CACHE_TIME') ?: '-5 minutes')
        );
        if ($cache->isValid()) {
            return $cache->getData();
        }

        $response = self::rawQuery($method, $params);
        $contents = $response->getBody()->getContents();
        $cache->setData($contents);

        return $contents;
    }

    /**
     * @param $albumId
     *
     * @return string
     */
    public static function getAlbum($albumId)
    {
        return self::query(
            'flickr.photosets.getPhotos',
            ['photoset_id' => $albumId]
        );
    }

    /**
     * @param $albumId
     *
     * @return string
     */
    public static function getAlbumThumbnailUrl($albumId)
    {
        $album = self::getAlbum($albumId);
        $album = json_decode($album)->photoset;

        $image = null;
        foreach ($album->photo as $photo) {
            if ($photo->id === $album->primary) {
                $image = $photo;
                break;
            }
        }

        return self::getStaticPhotoUrl($image, 'Small 320');
    }

    /**
     * For size values look here: https://www.flickr.com/services/api/flickr.photos.getSizes.html
     *
     * @param object $photo
     * @param string $size
     *
     * @return string
     */
    public static function getStaticPhotoUrl($photo, $size)
    {
        if (! $photo) {
            return '';
        }

        $size = self::convertSizeNameToShortTag($size);

        return "https://farm{$photo->farm}.staticflickr.com/{$photo->server}/{$photo->id}_{$photo->secret}_{$size}.jpg";
    }

    /**
     * @param string $size
     *
     * @return string
     */
    public static function convertSizeNameToShortTag($size)
    {
        return self::SIZES[$size];
    }

    /**
     * @param $albumId
     *
     * @return string
     */
    public static function getFlickrAlbumUrl($albumId)
    {
        $album = self::getAlbum($albumId);
        $album = json_decode($album)->photoset;

        return "https://www.flickr.com/photos/{$album->ownername}/albums/{$album->id}";
    }

    /**
     * @return bool
     */
    public static function isAvailable()
    {
        global $conn;
        $id = (mysqli_fetch_row($conn->query('SELECT flickr from gigadmin.gigs WHERE 1=1 LIMIT 1')))[0] ?? 1;
        $album = self::getAlbum($id);
        $response = json_decode($album);

        return ! ($response->stat == 'fail');
    }
}
