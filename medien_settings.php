<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 05.01.2019
 * Time: 17:05
 */

namespace JUZE\Medien\Settings;

use Dotenv\Dotenv;
use JUZE\Medien\Flickr\Flickr;
use PetrKnap\Php\Singleton\SingletonTrait;

class Settings
{
    use SingletonTrait;

    private static $settings;

    protected function __construct() {
        // load dotenv
        global $dotenv;
        $rootDir = dirname($_SERVER['DOCUMENT_ROOT']);
        $dotenv = Dotenv::create($rootDir);
        if (file_exists($rootDir . '/.env')) {
            $dotenv->load();
            $dotenv->required([
                'FLICKR_API_KEY',
                'SOLO_FLICKR_ALBUMS_PATH',
            ]);
        }

        global $conn;
        $query = "
          (SELECT datum FROM gigadmin.gigs WHERE flickr > 0 ORDER BY datum ASC LIMIT 1)
          UNION ALL
          (SELECT datum FROM gigadmin.gigs WHERE flickr > 0 ORDER BY datum DESC LIMIT 1)
          ";
        $result = $conn->query($query);
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

        self::$settings = [
            #'minYear' => intval(date('Y', strtotime($rows[0]['datum']))),
            'minYear' => 2012,
            'maxYear' => intval(date('Y', strtotime($rows[1]['datum']))),
            'flickrApiAvailable' => Flickr::isAvailable(),
        ];
    }


    public function get()
    {
        return self::$settings;
    }

    public function __get($name)
    {
        return self::$settings[$name];
    }
}

require_once dirname(dirname(__DIR__)) . '/gigadmin/db.php';
Settings::getInstance();
