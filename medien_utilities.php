<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 07.02.2019
 * Time: 15:58
 */

namespace JUZE\Medien\Utilities;

use JUZE\Medien\Flickr\Flickr;

require_once dirname(dirname(__DIR__)) . '/gigadmin/db.php';

class Utilities
{
    /**
     * Sends json response
     *
     * @param mixed $response
     */
    public static function sendResponse($response) {
        echo json_encode($response);
        exit;
    }

    /**
     * Queries db with prepared statements :)
     *
     * @param string $query
     * @param string $types
     * @param mixed  ...$params
     *
     * @return bool|\mysqli_result
     */
    public static function queryDB($query, $types, ...$params)
    {
        /**@var \mysqli $conn*/
        global $conn;

        $stmt = $conn->prepare($query);
        $stmt->bind_param($types, ...$params);
        $stmt->execute();

        return $stmt->get_result();
    }

    /**
     * Prepares the album to be returned by the api
     *
     * @param object $row
     *
     * @return object
     */
    public static function prepareAlbum($row)
    {
        $album = (object)[
            'id' => $row->flickr,
            'date' => date('d.m.Y', strtotime($row->datum)),
            'title' => self::getAlbumTitle($row),
        ];

        // thumbUrl
        $album->thumbUrl = Flickr::getAlbumThumbnailUrl($album->id) ?: '';

        return $album;
    }

    /**
     * Returns the formatted album title
     *
     * @param object $row
     *
     * @return string
     */
    public static function getAlbumTitle($row)
    {
        $title = $row->veranstaltung;

        if (! empty($row->albumtitel)) {
            $title = $row->albumtitel;
        } else if (! empty($row->band1name)) {
            $bands = [];
            for ($i = 1; $i <= 4; $i++) {
                $bandname = $row->{"band{$i}name"};
                if (empty($bandname)) { continue; }
                $bands[] = rtrim($bandname);
            }
            $title = html_entity_decode(implode(', ', $bands));
        }

        return self::shortenTitle($title);
    }

    /**
     * @param string $title
     *
     * @return bool|string
     */
    public static function shortenTitle($title)
    {
        $titleMaxLength = intval(\Env::get('TITLE_MAX_LENGTH') ?: 50);

        if (strlen($title) <= $titleMaxLength) {
            return $title;
        }

        return substr($title, 0, $titleMaxLength) . '...';
    }

    /**
     * Hashes the given params to get a unique identifier for the specific api call
     *
     * @param array $params
     *
     * @return string
     */
    public static function hashApiParams($params)
    {
        $hash = '';

        foreach ($params as $p) {
            if (is_array($p)) {
                $hash .= self::hashApiParams($p);
            } else {
                $hash .= md5($p);
            }
        }

        return md5($hash);
    }

    /**
     * Parses the credits string to replace url modifiers
     *
     * @param string $credits
     *
     * @return string
     */
    public static function parseCredits($credits)
    {
        return preg_replace(
            '/\[URL=(.+?)\](.+?)\[\/URL\]/i',
            '<a href="\1" target="_blank">\2</a>',
            html_entity_decode($credits)
        );
    }
}
