/**
 * Created by dweipert on 05.01.19.
 */

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const isProduction = process.env.NODE_ENV === 'production';

// main config
const config = {
    mode: isProduction ? 'production' : 'development',

    entry: {},

    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js', '.vue']
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', {
                            targets: {
                                browsers: ['> 1%', 'last 2 versions']
                            }
                        }]
                    ]
                },
                exclude: path.resolve(__dirname, 'node_modules')
            },
            {
                test: /\.s[ac]ss$/,
                loader: ExtractTextPlugin.extract({
                    use: [
                        { loader: 'css-loader' },
                        { loader: 'postcss-loader',
                            options: {
                                plugins: () => {
                                    let plugins = [
                                        require('autoprefixer')(),
                                    ];
                                    if (isProduction) {
                                        plugins.push(require('cssnano')({ preset: 'default' }));
                                    }
                                    return plugins;
                                }
                            } },
                        { loader: 'sass-loader' }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },

    plugins: [
        new VueLoaderPlugin(),
        new ExtractTextPlugin({
            filename: '[name].css'
        })
    ]
};

// entries
config.entry = {
    'medien_fotos': './src/main.js',
};

module.exports = config;
