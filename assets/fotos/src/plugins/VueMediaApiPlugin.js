import store from '../store/store';

const VueMediaPlugin = {
    install(Vue, options) {
        Vue.prototype.$mediaApi = {
            call: (action, data, cb) => {
                store.commit('setLoading', true);
                jQuery.post('/web/medien/medien_api.php', {action, ...data}, (res) => {
                    res = JSON.parse(res);
                    cb(res);
                    store.commit('setLoading', false);
                });
            },
        };
    }
};

export default VueMediaPlugin;
