import VueMediaApiPlugin from './plugins/VueMediaApiPlugin';
import App from './components/App';
import Albums from './components/Albums';
import Album from './components/Album';
import store from './store/store';

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueMediaApiPlugin);

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'albums',
            component: Albums,
        },
        {
            path: '/album/:id',
            name: 'album',
            component: Album,
        },
        {
            path: '/album/:id/:img',
            name: 'album-img',
            component: Album,
        }
    ],
});

window.medien_fotos =
new Vue({
    el: '#fotos',
    router,
    store,
    render: (h) => h(App),
});
