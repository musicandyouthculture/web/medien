<?php

namespace JUZE\Medien\StaticAlbums;

use JUZE\Medien\Flickr\Flickr;
use JUZE\Medien\Utilities\Utilities;
use Symfony\Component\Yaml\Yaml;

class StaticAlbums
{
    /**
     * Possible date formats used in the static albums list
     *
     * @var array
     */
    private const DATE_FORMATS = [
        'd.m.y',
        'Y',
    ];

    /**
     * @var array
     */
    private static $albums;

    /**
     * @return array
     */
    public static function load()
    {
        if (! self::$albums) {
            self::$albums = Yaml::parseFile(
                realpath($_SERVER['DOCUMENT_ROOT'] . '/' . \Env::get('SOLO_FLICKR_ALBUMS_PATH'))
            )['solo-flickr-albums'];
        }

        return self::$albums;
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        $albums = self::load();

        // adjust fields
        array_walk($albums, [self::class, 'prepareAlbum']);

        return $albums;
    }

    /**
     * @param int $year
     *
     * @return array
     */
    public static function getByDate($year)
    {
        $albums = self::load();

        // filter by year
        $albums = array_filter($albums, function (&$item) use ($year) {
            $timestamp = 0;
            foreach (self::DATE_FORMATS as $dateFormat) {
                $date = \DateTime::createFromFormat($dateFormat, $item['date']);
                if (! $date) {
                    continue;
                }

                $timestamp = $date->getTimestamp();
            }

            return date('Y', $timestamp) == $year;
        });
        $albums = array_values($albums);

        // adjust fields
        array_walk($albums, [self::class, 'prepareAlbum']);

        return $albums;
    }

    /**
     * @param $id
     *
     * @return bool|array
     */
    public static function getAlbum($id)
    {
        $albums = self::load();

        foreach ($albums as $album) {
            if ($album['id'] == $id) {
                return self::prepareAlbum($album);
            }
        }

        return false;
    }

    /**
     * Primarily used as callback function for array_walk
     *
     * @param array &$album
     *
     * @return array
     */
    private static function prepareAlbum(&$album)
    {
        $album['title'] = Utilities::shortenTitle(html_entity_decode($album['title']));
        $album['thumbUrl'] = Flickr::getAlbumThumbnailUrl($album['id']);

        return $album;
    }
}
