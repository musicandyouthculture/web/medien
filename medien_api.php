<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 04.01.2019
 * Time: 13:12
 */

namespace JUZE\Medien\API;

use JUZE\Medien\Flickr\Flickr;
use JUZE\Medien\Settings\Settings;
use JUZE\Medien\StaticAlbums\StaticAlbums;
use JUZE\Medien\Utilities\Utilities;

if (! isset($_POST['action'])) {
    return null;
}

require_once __DIR__ . '/vendor/autoload.php';
require_once dirname(dirname(__DIR__)) . '/gigadmin/db.php';
/**@var \mysqli $conn*/ global $conn;
Settings::getInstance();
call_user_func(__NAMESPACE__ . '\\' . $_POST['action']);

function getAlbums()
{
    global $conn;

    #$query = 'SELECT * FROM fotos.lychee_albums WHERE public = 1 ORDER BY sysstamp DESC';
    $query = 'SELECT * FROM gigadmin.gigs WHERE flickr > 0 ORDER BY datum DESC';
    $result = $conn->query($query);

    $albums = [];
    while ($row = mysqli_fetch_object($result)) {
        $album = Utilities::prepareAlbum($row);

        $albums[] = $album;
    }

    $albums = array_merge($albums, StaticAlbums::getAll());

    Utilities::sendResponse($albums);
}
function getAlbumsByDate()
{
    $year = intval($_POST['year']);

    $albums = [];
    if ($year >= 2016) {
        $query = 'SELECT * FROM gigadmin.gigs WHERE flickr > 0 AND year(datum) = ? ORDER BY datum DESC';
        $result = Utilities::queryDB($query, 'i', $year);

        while ($row = mysqli_fetch_object($result)) {
            $album = Utilities::prepareAlbum($row);

            $albums[] = $album;
        }
    } else {
        $albums = StaticAlbums::getByDate($year);
    }

    Utilities::sendResponse($albums);
}

function getAlbum()
{
    $id = intval($_POST['id']);

    $queryAlbum = 'SELECT * from gigadmin.gigs WHERE flickr = ?';
    $resultAlbum = Utilities::queryDB($queryAlbum, 'i', $id);

    $albumRow = mysqli_fetch_object($resultAlbum);
    $album = [];

    if (! $albumRow) {
        $album = StaticAlbums::getAlbum($id);
        $album = array_merge($album, [
            'images' => [],
            'credits' => '',
        ]);
    } else {
        $album = [
            'id' => $id,
            'date' => date('d.m.Y', strtotime($albumRow->datum)),
            'title' => Utilities::getAlbumTitle($albumRow),
            'images' => [],
            'credits' => Utilities::parseCredits($albumRow->credits),
        ];
    }

    $album['flickr'] = [
        'url' => Flickr::getFlickrAlbumUrl($id),
    ];

    $images = Flickr::getAlbum($album['id']);
    $images = json_decode($images)->photoset->photo;

    foreach ($images as $image) {
        $album['images'][] = [
            'id' => $image->id,
            'title' => $image->title,
            'thumbUrl' => Flickr::getStaticPhotoUrl($image, 'Large Square'),
            'url' => Flickr::getStaticPhotoUrl($image, 'Large'),
        ];
    }

    Utilities::sendResponse($album);
}
