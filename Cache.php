<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 08.06.19
 * Time: 00:12
 */

namespace JUZE\Medien;

class Cache
{
    private $namespace;
    private $filename;
    private $time;
    private $filepath;
    private $data;

    /**
     * Cache constructor.
     *
     * @param string $namespace
     * @param string $filename
     * @param int    $time
     */
    public function __construct($namespace, $filename, $time)
    {
        $this->namespace = $namespace;
        $this->filename = $filename;
        $this->time = $time;

        $this->filepath = __DIR__ . "/$namespace/$filename";
    }

    /**
     * @param int $time
     *
     * @return $this
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @param string $filepath
     *
     * @return $this
     */
    public function setFilePath($filepath)
    {
        $this->filepath = $filepath;

        return $this;
    }

    /**
     * @param mixed $data
     *
     * @return bool|int
     */
    public function setData($data)
    {
        $this->data = $data;

        if (! is_dir(dirname($this->filepath))) {
            mkdir(dirname($this->filepath), '0777', true);
        }

        return file_put_contents($this->filepath, $data);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        // TODO: check up on time comparison so we can set $time to strtotime("5 minutes") instead of the negative inverse

        return
            file_exists($this->filepath) &&
            $this->time < filemtime($this->filepath);
    }

    /**
     * @return bool
     */
    public function isExpired()
    {
        return ! $this->isValid();
    }

    /**
     * @return bool|string
     */
    public function getData()
    {
        return $this->data ?? file_get_contents($this->filepath);
    }
}
